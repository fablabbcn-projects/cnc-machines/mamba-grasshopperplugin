# MAMBA GRASSHOPPER PLUGIN

[link to the published plugin in food4rhino](https://www.food4rhino.com/en/app/mamba)

This plugin is designed to be used on almost any kind of generic 3D printer both FDM(fused deposition modelling) & paste printers (ceramic-biomaterials with pressure or motor control), up to large-scale Robotic Fabricators using FFF technologies and running from Gcode or for simple path generator on them.
Available to be used to prepare out of the box with defect values for ceramic 3d printer for the in-house ceramic printer build in a 'Plug and Play' for vase geometries, or in a more controlled and experimental manner with Custom input.

![](img/MambaImage.png)

*This should work with most major firmware(Marlin, Repetier, RepRap, etc) and Gcode Reader. If you encounter any problems related to Gcode, let us know.*

This plugin has been tested for more than 3 years at IAAC-FabLab Barcelona by hundreds of students & staff helping to improve it and make as easy to use as possible. By providing a comprehensive, yet easy-to-use plugin, this plug-in can be used by both non-experts (for example, architects and designers familiar with Grasshopper's parametric toolbox) as well as advanced users (who can experiment with the different utilities and GCode assembler to input directly spatial 3dimensional lines to generate print paths and vary te range of input parameters to customize their print design)

*password: tioneil*

## Credits:

This plugin was created for FabLab Barcelona and IAAC(Institute for Advanced Architecture of Catalonia) after 3 years of internal development.

    -Ashkan Foroughi Dehnavi for his heavy work on the print path gcode generator building lot of utilities and improving deeply the inner core of this tool.
    -Josep Marti for aide in testing roughly all versions of it.
    -Alex Dubor for his initial script reference on gcode parsing.
    -Daphne Gerodimou for all the huge work on documenting this plugin and visuals.
    -Santi Fuentemilla & Guillem Camprodon for supporting this development.
    -Park Hyun for optimizing gcode parsing scripts.

- Eduardo Chamorro Martin
